import React from 'react';
import '../css/App.scss';
import Accueil from "../components/Accueil";
import PokemonInformation from "../components/PokemonInformation";
import Ability from "../components/Ability";
import {
    Switch,
    Route,
    HashRouter
} from "react-router-dom";
import Move from "../components/Move";

class App extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            pokedexIsOpen: false
        };
        this.pressEnterToOpen = this.pressEnterToOpen.bind(this);
        this.changePokedexState = this.changePokedexState.bind(this);
    }

    changePokedexState(value){
        this.setState({
            pokedexIsOpen: value
        });
    }

    pressEnterToOpen(e) {
        //On fait une fonction qui agira sur l'évènement keydown sur la touche Enter

        //Comme l'évènement est sur toutes les touches, on lui demande d'agir uniquement
        //Sur la touche 13 qui est la touche Entrée du clavier
        if(e.keyCode === 13 && this.state.pokedexIsOpen === false){

            //Si c'est bien la touche entrée, on cible les éléments
            let $pressEnterToOpen = document.getElementById('pressEnterTo');
            let $titlePokedex = document.getElementById('title');
            let $tapToOpen = document.getElementById('tapToOpen');
            let $bannierePokedex = document.getElementById('banniereTitle');

            //Et on leur apporte les modifications
            $bannierePokedex.style.opacity = "0";
            $pressEnterToOpen.style.opacity = "0";
            $titlePokedex.style.opacity = "0";
            $tapToOpen.setAttribute('class', 'disable');

        }
    }

    render(){
        return (
            <HashRouter>
                <div className="App" onKeyDown={this.pressEnterToOpen} tabIndex="-1">
                    <div className="fondBleu">
                        <div className="divPokedex">
                            <img className="pokedex left" src="img/Pokedex-leftside.png" alt="pokedex"/>

                            <Switch>
                                <Route path={`/pokedex/:id`} component={PokemonInformation} />
                                <Route path={`/ability/:name`} component={Ability}/>
                                <Route path={`/move/:name`} component={Move}/>
                                <Route path="/">
                                    <Accueil isOpen={this.state.pokedexIsOpen} onChangePokedex={this.changePokedexState}/>
                                </Route>
                            </Switch>
                            <img className="pokedex right" src="img/Pokedex-rightside.png" alt="pokedex"/>
                        </div>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default App;
