import React from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";

import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

class PokemonInformation extends React.Component{
    constructor(props){
        super(props);
        this.state={
            infoPokemon: [],
            typePokemon: "",
            abilitiesPokemon: "",
            statPokemon: "",
            movePokemon: "",
            detailPokemon: [],
            descriptionPokemon: "",
            pokedexIsOpen: false,
        };

    }

    componentDidMount() {
        //Je récupère l'id passé en paramètre dans l'url sur lequel on a cliqué
        const { id } = this.props.match.params;
        //Je fais ensuite une requête sur l'API
        axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
            .then(res => {
                const infoPokemon = res.data;
                //console.log(res.data);
                this.setState({ infoPokemon });

                //TYPE
                const typePokemon = this.state.infoPokemon.types.map((t)=>{
                    return  `${t.type.name} - `
                });
                this.setState({ typePokemon });
                //ABILITIES
                const abilitiesPokemon = this.state.infoPokemon.abilities.map((a, index)=>{
                    return <NavLink to={`/ability/${a.ability.name}`} key={index}><li>{a.ability.name}</li></NavLink>
                });
                this.setState({ abilitiesPokemon });
                //STATS
                const statPokemon = this.state.infoPokemon.stats.map((s, index)=>{
                    return <p key={index}>{s.stat.name} : {s.base_stat}</p>
                });
                this.setState({ statPokemon });
                //MOVES
                const movePokemon = this.state.infoPokemon.moves.map((m, index)=>{
                    return <NavLink to={`/move/${m.move.name}`} key={index}><li>{m.move.name}</li></NavLink>
                });
                this.setState({ movePokemon });

                //DEUXIEME REQUETE
                axios.get(`https://pokeapi.co/api/v2/pokemon-species/${id}`)
                    .then(res2 => {
                        const detailPokemon = res2.data;
                        //console.log(res2.data);
                        this.setState({detailPokemon});

                        //DESCRIPTION
                        const descriptionPokemon = this.state.detailPokemon.flavor_text_entries[1].flavor_text;
                        //console.log(descriptionPokemon);
                        this.setState({ descriptionPokemon });
                    })
            })
    }
    render(){
        //Je réunis toutes les infos relatives au pokémon sur lequel on a cliqué
        //NOM
        const pokemonNameInfo = this.state.infoPokemon.name;
        //INFOS
        let pokemonImgInfo = "";
        let pokemonTypeInfo = "";
        let pokemonAbilitiesInfo = "";
        let pokemonStatInfo = "";
        let pokemonMoveInfo = "";
        //DETAILS
        let pokemonDescriptionInfo = "";

        if (this.state.infoPokemon.length !== 0){
            pokemonImgInfo = <img className="imgInfoPokemon" src={this.state.infoPokemon.sprites.front_default} alt={this.state.infoPokemon.name} />;
            pokemonTypeInfo = this.state.typePokemon;
            pokemonAbilitiesInfo = this.state.abilitiesPokemon;
            pokemonStatInfo = this.state.statPokemon;
            pokemonMoveInfo = this.state.movePokemon;
            pokemonDescriptionInfo = this.state.descriptionPokemon;
        }


        return (
            <div id="divPokemonInformation">
                <NavLink to="/"><img src="img/retour.png" alt="bouton retour"/></NavLink>
                <h2>INFORMATION ABOUT <span className="pokemonNameInfo">{pokemonNameInfo}</span></h2>
                <div id="divInfoGenerale">
                    <div id="imgInfo">
                        <div id="imgPkmn">
                            {pokemonImgInfo}
                        </div>
                        <div id="infoPkmn">
                            <h3>Info</h3>
                            <p><span>Type</span> : {pokemonTypeInfo}</p>
                            <p><span>Abilities</span> : {pokemonAbilitiesInfo}</p>
                        </div>
                    </div>
                    <div id="description">
                        <div id="descriptionPkmn">
                            <h3>Description</h3>
                                <SimpleBar style={{ maxHeight: "14vh" }}>
                                    {pokemonDescriptionInfo}
                                </SimpleBar>
                        </div>
                    </div>
                </div>
                <div id="statMoves">
                    <div id="statsPkmn">
                        <h3>Stats</h3>
                        {pokemonStatInfo}
                    </div>
                    <div id="movePkmn">
                        <h3>Moves</h3>
                        <div className="moves">
                            <SimpleBar style={{ maxHeight: "43vh" }}>
                                {pokemonMoveInfo}
                            </SimpleBar>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default PokemonInformation;
