import React from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";

class Ability extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            infoAbility: [],
            effectAbility: "",
        }
    }

    componentDidMount() {
        //On récupère la donnée passée en paramètre dans l'url
        const { name } = this.props.match.params;

        //Je fais ma requête à l'API
        axios.get(`https://pokeapi.co/api/v2/ability/${name}`)
            .then(res=>{
                const infoAbility = res.data;
                //console.log(infoAbility);
                this.setState({infoAbility});
                //EFFECT
                const effectAbility = this.state.infoAbility.effect_entries[0].effect;
                this.setState({effectAbility});

            })
    }

    render(){
        let nameAbility = this.state.infoAbility.name;
        let effectAbility = this.state.effectAbility;

        return(
            <div className="active">
                <NavLink to="/"><img src="img/retour.png" alt="bouton retour"/></NavLink>
                <h3>{nameAbility}</h3>
                <div id="ability">
                    <div className="effectAbility">
                        {effectAbility}
                    </div>
                    <div className="pikaGif">
                        <img src="img/pikachu.gif" className="pikaGif" alt="pikachu"/>
                    </div>
                </div>
            </div>
        )
    }
}
export default Ability;
