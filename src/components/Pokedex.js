import React from 'react';
import axios from 'axios';
import { NavLink } from "react-router-dom";
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

class Pokedex extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            listAllPokemon: [],
            infoPokemon: [],
        };
        this.onMouseOverHandle = this.onMouseOverHandle.bind(this);
    }
    componentDidMount() {
        axios.get(`https://pokeapi.co/api/v2/pokemon?limit=151`)
            .then(res => {
                const listAllPokemon = res.data.results;
                //console.log(res.data.results);
                this.setState({ listAllPokemon });
            })
    }
    onMouseOverHandle(e, url){
        e.preventDefault();
        axios.get(`${url}`)
            .then(res => {
                const infoPokemon = res.data;
                //console.log(res.data);
                this.setState({ infoPokemon });
            })
    }

    render(){
        //Ici on parcours le tableau de pokémon pour tous les afficher en liste
        const listPokemon = this.state.listAllPokemon.map((pokemon, index) => {
            return <NavLink to={`/pokedex/${index+1}`} key={`test-${index+1}`}><li onMouseOver={(e)=>this.onMouseOverHandle(e, pokemon.url)}>#{index+1} - {pokemon.name}</li></NavLink>
        }
        );
        //Ici, on affiche les sprites, en précisant que temps que la souris n'est pas passée sur la liste, rien n'est affiché
        let imgPokemon = "";
        if (this.state.infoPokemon.length !== 0){
            imgPokemon = <img className="logoPokemon" src={this.state.infoPokemon.sprites.front_default} alt={this.state.infoPokemon.name} />;
        }
        //On fait de même avec les informations
        let nameInfoPokemon = "";
        if (this.state.infoPokemon.length !== 0){
            nameInfoPokemon= "Name : "+this.state.infoPokemon.name;
        }
        //Parfois il y a des double types, alors on affiche pas les choses de la même façon
        let typesInfoPokemon = "";
        if (this.state.infoPokemon.length !== 0){
            if (this.state.infoPokemon.types.length ===2){
                typesInfoPokemon = "Types : "+this.state.infoPokemon.types[0].type.name+" - "+this.state.infoPokemon.types[1].type.name;
            }
            else {
                typesInfoPokemon = "Type : "+this.state.infoPokemon.types[0].type.name;
            }
        }
        //De même pour les capacités spéciales, on adapte l'affichage en fonction du nombre que le pokémon possède
        let abilityInfoPokemon = "";
        if (this.state.infoPokemon.length !==0){
            if (this.state.infoPokemon.abilities.length === 3){
                abilityInfoPokemon = "Abilities : "+this.state.infoPokemon.abilities[0].ability.name+" / "+this.state.infoPokemon.abilities[1].ability.name+" / "+this.state.infoPokemon.abilities[2].ability.name;
            }
            if (this.state.infoPokemon.abilities.length === 2){
                abilityInfoPokemon = "Abilities : "+this.state.infoPokemon.abilities[0].ability.name+" / "+this.state.infoPokemon.abilities[1].ability.name;
            }
            if (this.state.infoPokemon.abilities.length === 1){
                abilityInfoPokemon = "Ability : "+this.state.infoPokemon.abilities[0].ability.name;
            }
        }

        return (
            <div className="listPokedex">
                <div id="container">
                    <div id="infoSommairePokemon">
                        <div className="imgPokemon">
                            {imgPokemon}
                        </div>
                        <div className="slimInfoPokemon">
                            <h2>Description</h2>
                            <div className="info">
                                {nameInfoPokemon}<br/><br/>
                                {typesInfoPokemon}<br/><br/>
                                {abilityInfoPokemon}
                            </div>
                        </div>
                    </div>
                    <div id="listPokemonPokedex">
                        <h2>Pokemon</h2>
                        <ul>
                            <SimpleBar style={{ maxHeight: "77vh" }}>
                            {listPokemon}
                            </SimpleBar>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Pokedex;
