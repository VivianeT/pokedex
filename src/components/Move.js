import React from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";

class Move extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            infoMove: [],
            effectMove: "",
            contestTypeMove: "",
            damageClassMove: "",
            targetMove: "",
            typeMove: "",
        }
    }

    componentDidMount() {
        //On récupère la donnée passée en paramètre dans l'url
        const { name } = this.props.match.params;

        //Je fais ma requête à l'API
        axios.get(`https://pokeapi.co/api/v2/move/${name}`)
            .then(res=>{
                const infoMove = res.data;
                console.log(infoMove);
                this.setState({infoMove});
                //EFFECT
                const effectMove = this.state.infoMove.effect_entries[0].effect;
                this.setState({effectMove});
                //CONTEST TYPE
                const contestTypeMove = this.state.infoMove.contest_type.name;
                this.setState({contestTypeMove});
                //DAMAGE CLASS
                const damageClassMove = this.state.infoMove.damage_class.name;
                this.setState({damageClassMove});
                //TARGET
                const targetMove = this.state.infoMove.target.name;
                this.setState({targetMove});
                //TYPE
                const typeMove = this.state.infoMove.type.name;
                this.setState({typeMove});
            })
    }

    render(){
        //INFO DE BASE
        let nameMove = this.state.infoMove.name;
        let effectMove = this.state.effectMove;
        let contestMoveType = this.state.contestTypeMove;
        let damageClassMove = this.state.damageClassMove;
        let ppMove = this.state.infoMove.pp;
        let priorityMove = this.state.infoMove.priority;
        let targetMove = this.state.targetMove;
        let typeMove = this.state.typeMove;

        //ACCURACY
        let accuracyMove = this.state.infoMove.accuracy;
        if(accuracyMove === null){
            accuracyMove = "///";
        }

        //POWER
        let powerMove = this.state.infoMove.power;
        if(powerMove === null){
            powerMove = "///";
        }


        return(
            <div className="active">
                <NavLink to="/"><img src="img/retour.png" alt="bouton retour"/></NavLink>
                <h3>{nameMove}</h3>
                <div id="moveDescription">
                    <div className="effect">
                        {effectMove}
                    </div>
                    <div className="infoMove">
                        <div>
                            <p><span>Accuracy</span> : {accuracyMove}</p>
                            <p><span>Damage Class</span> : {damageClassMove}</p>
                            <p><span>Power</span> : {powerMove}</p>
                            <p><span>PP</span> : {ppMove}</p>
                        </div>
                        <div>
                            <p><span>Priority</span> : {priorityMove}</p>
                            <p><span>Target</span> : {targetMove}</p>
                            <p><span>Type</span> : {typeMove}</p>
                            <p><span>Contest Type</span> : {contestMoveType}</p>
                        </div>
                    </div>
                    <div>
                        <img src="img/pikachu2.gif" alt="pikachu"/>
                    </div>
                </div>

            </div>
        )
    }
}

export default Move;
