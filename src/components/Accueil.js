import React from 'react';
import Pokedex from "./Pokedex";

class Accueil extends React.Component {
  constructor(props){
      super(props);
      this.state = {
          openPokedex: "Tap to open",
          openListPokemon: "Press Enter to Open Pokedex",
      };
      this.transitionEnd = this.transitionEnd.bind(this);
  }

  handleOpen(e) {
      //On fait une fonction pour ouvrir le pokédex et afficher l'écran d'accueil

      //On cible les éléments qui nous intéressent
      let $pressEnterToOpen = document.getElementById('pressEnterTo');
      let $titlePokedex = document.getElementById('title');
      let $pokedexContent = document.getElementById('content');
      let $tapToOpen = document.getElementById('tapToOpen');
      let $bannierePokedex = document.getElementById('banniereTitle');

      //On les rend opaque et on les repositionne
      $pokedexContent.classList.add('active');
      $tapToOpen.style.opacity = "0";
      $titlePokedex.style.opacity = "1";
      $titlePokedex.style.top = "8vh";
      $bannierePokedex.style.opacity = "1";
      $pressEnterToOpen.style.opacity = "1";
  }

  transitionEnd(e){
      let $titlePokedex = document.getElementById('title');
      let $pressEnterToOpen = document.getElementById('pressEnterTo');
      let $bannierePokedex = document.getElementById('banniereTitle');
      //On ouvre le pokédex pour pouvoir passer ce paramètre dans le prochain component
      this.props.onChangePokedex(true);
      $titlePokedex.setAttribute('class', 'disable');
      $pressEnterToOpen.setAttribute('class', 'disable');
      $bannierePokedex.setAttribute('class', 'disable');
  }

  //Ici la fonction render retourn la page d'accueil avec un pokédex fermé qui doit s'ouvrir
    //Sur un écran de jeu, puis on presse Entrée pour continuer et accéder au Pokédex
  render(){
      let maClasse = "";
      let pokedex = null;
      if(this.props.isOpen === true) {
          pokedex = (<Pokedex isOpen={this.props.pokedexIsOpen}/>);
          maClasse="active"
      }

      return (
                  <div id="content" className={maClasse}>
                      <img id="title" src="img/pokemon.png" alt="title"/>
                      <img id="banniereTitle" src="img/bann.png" alt="bannière" onTransitionEnd={this.transitionEnd}/>
                      <div id="tapToOpen" onClick={this.handleOpen}>
                          {this.state.openPokedex}
                      </div>
                      <div id="pressEnterTo">
                          {this.state.openListPokemon}
                      </div>
                      <div>
                          {pokedex}
                      </div>
                  </div>
      )
  }
}

/*On l'exporte*/
export default Accueil;

/* Et du coup pour utiliser le this.props.content, il faut avoir fait une insersion de content="pouet", pour que son
* appel se fasse
* */
