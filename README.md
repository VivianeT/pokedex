# Pokedex

## What is it

This project is a Pokedex made with React that can be seen live [here](https://www.pokedex.vivianetixier.com).
Content is taken from a specific API ([PokéAPI](https://pokeapi.co/)).

[![Before entering the Pokedex](example1.png)](https://www.pokedex.vivianetixier.com)

<!> Be aware that this project is still under construction <!>


## How to build

 - Install NPM if you don't have it

```
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt-get -y -qq install nodejs > /dev/null
```

 - Get required packages for the projet

`npm install`

 - Build the project

`npm run build`

 - Put `build` folder into at the root of your web server or run `npm run start` if you are on development mode.


## License

This project is under MIT license. This means you can use it as you want (credit me please).
